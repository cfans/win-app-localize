﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CF_Language_Switch
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            cbLang.SelectedIndex = getLanguageIndex();
            cbLang.SelectionChanged += new SelectionChangedEventHandler(comboBox_SelectionChanged);

        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem item = (ComboBoxItem)cbLang.SelectedItem;
            //Console.WriteLine("" + item.Content + " : "+cbLang.Text+"index "+cbLang.SelectedIndex);
            switchLanguage(cbLang.SelectedIndex);
        }

        private int getLanguageIndex()
        {
            int index = 0;
            if (App.Culture.ToString().Equals("en-US"))
            {
                index = 1;
            }
            return index;
        }

        private void switchLanguage(int index)
        {
            if (index == 0)
            {
                App.Culture = "zh-CN";
            }
            else
            {
                App.Culture = "en-US";
            }
            //update culture
            App.UpdateCulture();
        }
    }
}
