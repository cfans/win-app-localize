﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CF_Language_Switch
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            GetCulture();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            SetCulture();
        }

        internal static string Culture;

        #region Method
        private void GetCulture()
        {
            Culture = string.Empty;
            try
            {
                Culture = CF_Language_Switch.Properties.Settings.Default.Culture.Trim();
            }
            catch (Exception)
            {
            }
            Culture = string.IsNullOrEmpty(Culture) ? "en-US" : Culture;

            //update culture
            UpdateCulture();
        }
        private void SetCulture()
        {
            try
            {
                CF_Language_Switch.Properties.Settings.Default.Culture = Culture;
                CF_Language_Switch.Properties.Settings.Default.Save();
            }
            catch (Exception)
            {
            }

        }
        internal static void UpdateCulture()
        {
            List<ResourceDictionary> dictionaryList = new List<ResourceDictionary>();
            foreach (ResourceDictionary dictionary in Application.Current.Resources.MergedDictionaries)
            {
                dictionaryList.Add(dictionary);
            }
            string requestedCulture = string.Format(@"String\String.{0}.xaml", Culture);
            ResourceDictionary resourceDictionary = dictionaryList.FirstOrDefault(d => d.Source.OriginalString.Equals(requestedCulture));
            if (resourceDictionary == null)
            {
                requestedCulture = @"String\String.xaml";
                resourceDictionary = dictionaryList.FirstOrDefault(d => d.Source.OriginalString.Equals(requestedCulture));
            }
            if (resourceDictionary != null)
            {
                Application.Current.Resources.MergedDictionaries.Remove(resourceDictionary);
                Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
            }

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Culture);
        }
        #endregion
    }

}
